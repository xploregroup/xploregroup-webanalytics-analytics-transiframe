/*!
* AnalyticsTransIFrame.js Library v1.0.1
*
* Copyright 2017, Stefan Maris
* MIT Licensed (http://www.opensource.org/licenses/mit-license.php)
*
*
* Last update: December 5, 2017
*/
(function (root, factory) {

  'use strict';

  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(["analyticstracker"], function (analyticstracker) {
			return (root.analyticsTransIFrame = factory(analyticstracker, window));
		});
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like enviroments that support module.exports,
    // like Node.
    root.analyticsTransIFrame = factory(require('analyticstracker'), window)
    module.exports = root.analyticsTransIFrame;
  } else {
    // Browser globals
    root.analyticsTransIFrame = factory(root.analyticstracker, window);
  }

}(this, function (analyticstracker, w) {
// UMD Definition above, do not remove this line
  'use strict';

  var _instance;

  var analyticsTransIFrame = function analyticsTransIFrame() {
    if (!(this instanceof analyticsTransIFrame)) {
      return new analyticsTransIFrame();
    }

    this.tracker = analyticstracker();
    this.parentURLs = [];
    this._version = "1.0.1";
    this._subscribe();
  }

  analyticsTransIFrame.prototype.getInstance = function() {
    if (!_instance) {
        _instance = analyticsTransQA();
    }
    return _instance;
  }

  analyticsTransIFrame.prototype._subscribe = function() {
    try {
      this.tracker.trackingSubscribe("IFrame", function (event, data) {
        if (! Array.isArray(_instance.parentURLs)) {
          throw new Error("Array expected");
          return false;
        }
        if (_instance.parentURLs.length > 0) {
          if (event == "page-impression") {
            event = "iframe-impression";
            data.event = "iframe-impression";
          }
          console.log("IFrame tracker: " + event);
          console.log(data);
          try {
            if ((typeof w != "undefined") && (w != null)) {
              // clone sending pageInfo in iframeInfo, and delete pageInfo, 
              // since pageInfo is in receiving parent
              data.iframeInfo = JSON.parse(JSON.stringify(data.pageInfo));
              delete data.pageInfo;
              var postObject = JSON.stringify(data);
              _instance.parentURLs.forEach( function ( pUrl ) {
                w.parent.postMessage(postObject, pUrl);
              });
            }
          } catch (err) {
            console.log("IFrame tracker ERROR: " + err.name + ": " + err.message);
          }
          console.log("********");
        }
      });
    } catch (e) {
      console.log("IFrame tracker subscribing ERROR: " + e.name + ": " + e.message);
    }
  }

  analyticsTransIFrame.prototype.addParentUrl = function (pUrl) {
    if (! this.parentURLs.includes(pUrl)) {
      this.parentURLs.push(pUrl);
    }
  }

  _instance = analyticsTransIFrame();
  return analyticsTransIFrame.prototype.getInstance;
}));
