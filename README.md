# Analytics-TransIframe

Analytics-transIFrame is a client-side Javascript module for translating events to parent IFrame tracking via window.PostMessage in the Analyticstracker module for handling the [Event Driven Web Analytics Tracking](https://bitbucket.org/xploregroup/xploregroup-webanalytics-demo).

## Prerequisites

Analytics-TransIFrame is depending on [mediator-js](http://thejacklawson.com/Mediator.js/) and the [Analyticstracker](https://bitbucket.org/xploregroup/xploregroup-webanalytics-analyticstracker)

## Getting Started

Download the script and load it directly in HTML with a script tag:

```
<script type="text/javascript" src="mediator.min.js"></script>
<script type="text/javascript" src="analyticstracker.min.js"></script>
<script type="text/javascript" src="analyticsTransQA.min.js"></script>
```

or require it in your preferred script loader (e.g. require.js)

```
require(['mediator-js', 'analyticstracker', 'analyticsTransQA'], function($) {
	if (module = $('script[src$="require.js"]').data('module')) {
		require([module]);
	}
});
```

and start tracking:

```
var atrack = analyticstracker();

$(document).ready(function(){
  atrack.trackImpression("page-impression");
});

$('[data-tracking-event$=-click]').click(atrack.trackInteraction);
```

The Translator will subscribe automatically to the Analyticstracker, and receives all pre-recorded events.

## Methods

**analyticsTransIFrame()**

Is a Singleton implementation of the translator. Loading the module will create an instance.

**addParentUrl (url)**

Adds an URL to the list of allowed parents for this iFrame (see analyticstracker iFrame tracking for explanation).


## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Changelog

##### version 1.0.1
> Fixed issue with parentURLs list. only add URL's once

##### version 0.1.0
> Intitial version

## Authors

* **Stefan Maris** - *Initial work* - [Xplore Group](http://www.xploregroup.be)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Based on POC implementation at Essent.nl
